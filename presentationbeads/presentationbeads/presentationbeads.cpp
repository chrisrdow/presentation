// presentationbeads.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <math.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <fstream>      // std::ifstream

#define PI (float)3.1415926535897

using namespace cv;
using namespace std;

void averageInsity(Mat image, float &average_i) {

	int sum = 0;
	int npixels = 0;
	int thresh = 800;

	for (int j = 0; j < image.rows; j++) {
		for (int i = 0; i < image.cols; i++) {

			if ((int)image.at<unsigned short>(i, j) > thresh) {
				sum += (int)image.at<unsigned short>(i, j);
				npixels++;
			}
		}
	}
	if (npixels == 0) {
		average_i = 0;

	}
	else {
		average_i = (float)(sum) / (float)npixels;
	}
}


int main()
{
	Mat image_r, image_g;
	Mat b, b0, b1;
	Mat b_r, b0_r, b1_r;
	Mat bgr_g[3];
	Mat bgr_gg[3];
	Mat bgr_r[3];
	Rect roi_gg;
	Rect roi_g;
	Rect roi_red;
	string line_r, line_g, filename;
	ifstream r_ifs;
	ifstream g_ifs;
	ofstream gofs,rofs;
	stringstream convert;

	string folder = "set2";

	string green_in = folder + "/green.txt";
	string red_in =  folder + "/red.txt";
	string green_out = folder + "/green.csv";
	string red_out =  folder + "/red.csv";
	
	string red_fn;
	string green_fn;

	string rprefix = "11,500,";
	string gprefix = "11,500,";
	
	vector<Vec3f> circles_g,circles_r;

	g_ifs.open(green_in, ifstream::in);
	r_ifs.open(red_in, ifstream::in);

	gofs.open(green_out, ofstream::app);
	rofs.open(red_out, ofstream::app);
	
	

	if ((g_ifs.is_open())&& (rofs.is_open())&& (gofs.is_open())&& (r_ifs.is_open()))
	{
		

		rofs <<"tube"<< ","<< "sample" << "," << "file name" << "," << "mean red" << "," << "mean grean" << "," 
			 << "x coord"   << "," << "y coord"  << "," << "radius" << endl;

		gofs << "tube" << "," << "sample" << "," << "file name" << "," << "mean red" << "," << "mean grean" << ","
			 << "x coord"   << "," << "y coord"  << "," << "radius" << endl;

		
		
		int nsamples = 0;

		while (r_ifs.good())
		{
			
			getline(r_ifs, line_r);
			getline(g_ifs, line_g);
			image_r = imread(line_r, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);   // Read the file
			image_g = imread(line_g, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
			
			
			
			if ((image_r.data)&&(image_r.data))          // Check for invalid input
			{
				split(image_r, bgr_r);
				split(image_g, bgr_g);


				bgr_r[1].convertTo(b0, CV_8U, (float)(255.0 / 65535.0), 1.0);
				bgr_r[2].convertTo(b1, CV_8U, (float)(255.0 / 65535.0), 1.0);
			//	b0 = bgr_r[1];
			//	b1 = bgr_r[2];

				b = b0 + b1;
			//	imshow("", b);
			//	waitKey(0);
				HoughCircles(b, circles_r, CV_HOUGH_GRADIENT, 1, 10, 75, 4, 4, 6);
				int rbeads = 0;
				

				for (size_t i = 0; i < circles_r.size(); i++) {

					Point center(cvRound(circles_r[i][0]), cvRound(circles_r[i][1]));
					int radius = cvRound(circles_r[i][2]);

					
					roi_red.x = center.x - 5;
					roi_red.y = center.y - 5;
					roi_red.width = 10;
					roi_red.height = 10;


					float green_av = 0.0;
					float red_av = 0.0;
					
					if ((roi_red.x > 0) && (roi_red.x + roi_red.width < image_r.cols) &&
						(roi_red.y > 0) && (roi_red.y + roi_red.height < image_r.rows)) {

						Mat crop_r0 = bgr_r[2](roi_red);
						Mat crop_g0 = bgr_r[1](roi_red);

						averageInsity(crop_r0, red_av);
						averageInsity(crop_g0, green_av);


						convert << rprefix;
						convert << nsamples;
						convert << "_";
						convert << "red_bead_";
						convert <<  rbeads++;
						convert << ".tiff";
						convert << ",";
						convert << red_av;
						convert << ",";
						convert << green_av;
						convert << ",";
						convert << center.x;
						convert << ",";
						convert << center.y;
						convert << ",";
						convert << radius;

					
						string red_entry = convert.str();
						convert.clear();//clear any bits set
						convert.str(string());
						rofs << red_entry;
						rofs << std::endl;

						convert << folder;
						convert << "/red/i_" << nsamples << "_" << rbeads << ".tiff";
						string red_file = convert.str();
						convert.clear();//clear any bits set
						convert.str(string());


						vector<int> params = {
							259, 1//,     // No compression, turn off default LZW 
						};

						Mat crop = image_r(roi_red);
						imwrite(red_file, crop, params);

						

						roi_g.x = center.x - 100;
						roi_g.y = center.y - 100;
						roi_g.height = 200;
						roi_g.width  = 200;


						int gbeads = 0;

						if ((roi_g.x > 0) && (roi_g.x + roi_g.width < image_g.cols) &&
							(roi_g.y > 0) && (roi_g.y + roi_g.height < image_g.rows)) {


							Mat crop_g = image_g(roi_g);
							split(crop_g, bgr_g);

							//imshow("", crop_g);
							//waitKey(0);

							Mat b0_g ;
							Mat b1_g ;
							bgr_g[1].convertTo(b0_g, CV_8U, (float)(255.0 / 65535.0), 1.0);
							bgr_g[2].convertTo(b1_g, CV_8U, (float)(255.0 / 65535.0), 1.0);
							
							Mat b_g = b0_g + b1_g;

							HoughCircles(b_g, circles_g, CV_HOUGH_GRADIENT, 1, 10, 75, 4, 4, 6);

						
							for (size_t ii = 0; ii < circles_g.size(); ii++) {

								Point center_g(cvRound(circles_g[ii][0]), cvRound(circles_g[ii][1]));
								int radius_g = cvRound(circles_g[ii][2]);

								roi_gg.x = center_g.x - 5;
								roi_gg.y = center_g.y - 5;
								roi_gg.width = 10;
								roi_gg.height = 10;

								if ((roi_gg.x > 0) && (roi_gg.x + roi_gg.width  < crop_g.cols) &&
									(roi_gg.y > 0) && (roi_gg.y + roi_gg.height < crop_g.rows)) {
									

									Mat ig = crop_g(roi_gg);
									split(ig, bgr_gg);

									Mat crop_r1 = bgr_gg[2];
									Mat crop_g1 = bgr_gg[1];

									averageInsity(crop_r1, red_av);
									averageInsity(crop_g1, green_av);

									convert << gprefix;
									convert << "green_bead_";
									convert << nsamples;
									convert << "_";
									convert << rbeads;
									convert << "_";
									convert << gbeads++;
									convert << ".tiff";
									convert << ",";
									convert << red_av;
									convert << ",";
									convert << green_av;
									convert << ",";
									convert << center.x + center_g.x-100;
									convert << ",";
									convert << center.y + center_g.y-100;
									convert << ",";
									convert << radius_g;

									string green_entry = convert.str();
									convert.clear();//clear any bits set
									convert.str(string());
									gofs << green_entry;
									gofs << std::endl;

									convert << folder;
									convert << "/green/i_" << nsamples << "_" << rbeads << "_" << gbeads << ".tiff";
									string green_file = convert.str();
									convert.clear();//clear any bits set
									convert.str(string());


									vector<int> params = {
										259, 1//,     // No compression, turn off default LZW 
									};

									imwrite(green_file, ig,params);
								//	gbeads++;


								}
								
								

							}
						}
					}

				}
			
			}

			nsamples++;
			
		}
		 
	}
	r_ifs.close();
	g_ifs.close();
	rofs.close();
	gofs.close();

	
    return 0;
}

